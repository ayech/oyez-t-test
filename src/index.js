import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { BrowserRouter, Router, Switch } from "react-router-dom";
import createBrowserHistory from "history/createBrowserHistory";
import store from "./store";
import Routes from "./routes";

const history = createBrowserHistory();
const Root = () => {
  return (
    <Provider store={store()}>
      <BrowserRouter>
        <Router history={history}>
          <Switch>
            <Routes />
          </Switch>
        </Router>
      </BrowserRouter>
    </Provider>
  );
};

ReactDOM.render(<Root />, document.getElementById("root"));
