import React, { Component } from "react";
import { connect } from "react-redux";
import { addToWishlist, removeFromWishlist } from "../actions/wishlist";

class PhotoDetails extends Component {
  constructor() {
    super();
    this.state = {
      wishlisted: false
    };
  }
  componentWillMount() {
    const wishlisted = this.props.wishlist.find(
      photo => photo.id === this.props.currentPhoto.id
    );
    if (wishlisted) {
      this.setState({
        wishlisted: true
      });
    }
  }
  wishListThis = () => {
    this.setState(
      {
        wishlisted: true
      },
      () => {
        this.props.addToWishlist(this.props.currentPhoto);
      }
    );
  };

  unWishListThis = () => {
    const photoIndexInWishlist = this.props.wishlist.findIndex(photo => {
      return photo.id === this.props.currentPhoto.id;
    });
    this.setState(
      {
        wishlisted: false
      },
      () => {
        this.props.removeFromWishlist(photoIndexInWishlist);
      }
    );
  };
  render() {
    const container = {
      textAlign: "center"
    };
    const imgWrapper = {
      position: "relative",
      width: 600,
      height: 600,
      margin: "0 auto"
    };
    const btnWrapper = {
      position: "absolute",
      bottom: 20,
      width: "100%",
      display: "flex",
      justifyContent: "center"
    };
    const wishlistedBtn = {
      textTransform: "uppercase",
      background: "#2ecc71",
      border: "none",
      height: 40,
      width: 150,
      borderRadius: 5,
      fontFamily: "onsolas,sans-serif",
      color: "#fff",
      fontWeight: "bold",
      cursor: "pointer"
    };
    const unWishlistedBtn = { ...wishlistedBtn, background: "red" };
    const photo = this.props.currentPhoto;
    return (
      <div style={container}>
        <h1>Photo details</h1>
        <div style={imgWrapper}>
          <img src={photo.url} alt="photo" />
          <div style={btnWrapper}>
            {!this.state.wishlisted ? (
              <button onClick={this.wishListThis} style={wishlistedBtn}>
                add to wishlist
              </button>
            ) : (
              <button onClick={this.unWishListThis} style={unWishlistedBtn}>
                remove from wishlist
              </button>
            )}
          </div>
        </div>
        <h3>Photo title: {photo.title} </h3>
        <h5>
          Album title:
          {this.props.currentAlbum.title}{" "}
        </h5>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    currentPhoto: state.photos.currentPhoto,
    currentAlbum: state.albums.currentAlbum,
    wishlist: state.wishlist
  };
};
export default connect(
  mapStateToProps,
  { addToWishlist, removeFromWishlist }
)(PhotoDetails);
