import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { currentPhoto, fetchPhotos } from "../actions/photos";
import { Link } from "react-router-dom";
class Photos extends Component {

  componentDidMount() {
    this.props.fetchPhotos(this.props.currentAlbum.id);
  }
  handlePhotoClick = photo => {
    this.props.currentPhoto(photo);
  };
  render() {
    const container = {
      textAlign: "center"
    };
    const photosWrapper = {
      marginTop: 100,
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center"
    };
    return (
      <div style={container}>
        <h1> Album: {this.props.currentAlbum.title}</h1>
        <div style={photosWrapper}>
          {this.props.photos.length > 0 ? (
            <OnePhoto
              photos={this.props.photos}
              handlePhotoClick={this.handlePhotoClick}
            />
          ) : (
            <h1>Loading photos ...</h1>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos.photos,
    currentAlbum: state.albums.currentAlbum,
  };
};
export default connect(
  mapStateToProps,
  { fetchPhotos, currentPhoto }
)(Photos);

const OnePhoto = props => {
  const photoStyle = {
    height: 250,
    width: 250
  };
  const imgStyle = {
    borderRadius: 5
  };
  const { photos, handlePhotoClick } = props;
  return (
    <Fragment>
      {photos.map((photo, key) => {
        return (
          <Link
            to={"/photo/details"}
            onClick={() => handlePhotoClick(photo)}
            key={key}
            style={photoStyle}
          >
            <div>
              <img src={photo.thumbnailUrl} alt="thumbnail" style={imgStyle} />
            </div>
          </Link>
        );
      })}
    </Fragment>
  );
};
