import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { currentAlbum, fetchAlbums } from "../actions/albums";
class Albums extends Component {
  componentDidMount() {
    this.props.fetchAlbums();
  }
  handleAlbumClick = album => {
    this.props.currentAlbum(album);
  };
  render() {
    const container = {
      textAlign: "center"
    };
    const albumsWrapper = {
      marginTop: 100,
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center"
    };
    return (
      <div style={container}>
        <h1>Album gallery</h1>
        <div style={albumsWrapper}>
          {this.props.albums.length > 0 ? (
            <AnAlbum
              albums={this.props.albums}
              handleAlbumClick={this.handleAlbumClick}
            />
          ) : (
            <h1>Loading albums ...</h1>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    albums: state.albums.albums
  };
};
export default connect(
  mapStateToProps,
  { fetchAlbums, currentAlbum }
)(Albums);

const AnAlbum = props => {
  const albumStyle = {
    height: 150,
    width: 150,
    margin: 10,
    borderRadius: 5,
    padding: 20,
    border: "1px solid #e2e2e2",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    cursor: "pointer",
    textDecoration: "none",
    color: "#000"
  };
  const { albums, handleAlbumClick } = props;
  return (
    <Fragment>
      {albums.map((album, key) => {
        return (
          <Link
            to={"/photo"}
            onClick={() => handleAlbumClick(album)}
            key={key}
            style={albumStyle}
          >
            {album.title}
          </Link>
        );
      })}
    </Fragment>
  );
};
