import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import {currentPhoto} from "../actions/photos";

class Wishlist extends Component {
  handlePhotoClick = photo => {
    this.props.currentPhoto(photo);
  };
  render() {
    const container = {
      textAlign: "center"
    };
    const photosWrapper = {
      marginTop: 100,
      display: "flex",
      flexWrap: "wrap",
      justifyContent: "center"
    };
    return (
      <div style={container}>
        <h1> Wishlist</h1>
        <div style={photosWrapper}>
          {this.props.wishlist.length > 0 ? (
            <OnePhoto
              photos={this.props.wishlist}
              handlePhotoClick={this.handlePhotoClick}
            />
          ) : (
            <h1>No wishlisted photos</h1>
          )}
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    wishlist: state.wishlist
  };
};

export default connect(
  mapStateToProps,
  {currentPhoto}
)(Wishlist);

const OnePhoto = props => {
  const photoStyle = {
    height: 250,
    width: 250
  };
  const imgStyle = {
    borderRadius: 5
  };
  const { photos, handlePhotoClick } = props;
  return (
    <Fragment>
      {photos.map((photo, key) => {
        return (
          <Link
            to={"/photo/details"}
            onClick={() => handlePhotoClick(photo)}
            key={key}
            style={photoStyle}
          >
            <div>
              <img src={photo.thumbnailUrl} alt="thumbnail" style={imgStyle} />
            </div>
          </Link>
        );
      })}
    </Fragment>
  );
};
