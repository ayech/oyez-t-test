import { combineReducers } from "redux";
import albums from "./albums";
import photos from "./photos";
import wishlist from "./wishlist";

const rootReducer = combineReducers({
  albums,
  photos,
  wishlist
});

export default rootReducer;
