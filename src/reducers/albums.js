import {
  CURRENT_ALBUM,
  FETCH_ALBUMS_FAILURE,
  FETCH_ALBUMS_REQUEST,
  FETCH_ALBUMS_SUCCESS
} from "../actions/actionTypes";
const initialState = {
  albums: [],
  currentAlbum: {}
};
export default function albums(state = initialState, action) {
  switch (action.type) {
    case FETCH_ALBUMS_REQUEST:
      return state;
    case FETCH_ALBUMS_SUCCESS:
      return { ...state, albums: action.payload };
    case FETCH_ALBUMS_FAILURE:
      return { state, error: action.error };
    case CURRENT_ALBUM:
      return { ...state, currentAlbum: action.payload };
    default:
      return state;
  }
}
