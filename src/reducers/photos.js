import {
  CURRENT_PHOTO,
  FETCH_PHOTOS_FAILURE,
  FETCH_PHOTOS_REQUEST,
  FETCH_PHOTOS_SUCCESS
} from "../actions/actionTypes";
const initialState = {
  photos: [],
  currentPhoto: {}
};
export default function albums(state = initialState, action) {
  switch (action.type) {
    case FETCH_PHOTOS_REQUEST:
      return state;
    case FETCH_PHOTOS_SUCCESS:
      return { ...state, photos: action.payload };
    case FETCH_PHOTOS_FAILURE:
      return { state, error: action.error };
    case CURRENT_PHOTO:
      return { ...state, currentPhoto: action.payload };
    default:
      return state;
  }
}
