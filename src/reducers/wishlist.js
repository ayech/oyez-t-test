import {
  ADDED_TO_WISHLIST,
  REMOVED_FROM_WISHLIST
} from "../actions/actionTypes";

export default function wishlist(state = [], action) {
  switch (action.type) {
    case ADDED_TO_WISHLIST:
      return [...state, action.payload];
    case REMOVED_FROM_WISHLIST:
      return [
        ...state.slice(0, action.payload),
        ...state.slice(action.payload + 1)
      ];
    default:
      return state;
  }
}
