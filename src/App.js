import React, { Component } from "react";
import { Link } from "react-router-dom";

class App extends Component {
  render() {
    const container = {
      padding: "10px 50px"
    };
    const linksWrapper = {
      display: "flex",
      justifyContent: "space-between"
    };
    return (
      <div style={container}>
        <div style={linksWrapper}>
          <Link to={"/"} style={{ marginBottom: 50 }} >
            Home
          </Link>
          <Link to={"/wishlist"} style={{ marginBottom: 50 }}>Wishlit</Link>
        </div>
        {this.props.children}
      </div>
    );
  }
}

export default App;
