import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers/rootReducer";
import thunk from "redux-thunk";
import { createLogger } from "redux-logger";

const logger = createLogger({
  collapsed: true
});

export default function configureStore() {
  return createStore(rootReducer, applyMiddleware(thunk, logger));
}
