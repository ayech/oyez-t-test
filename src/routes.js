import React from "react";
import { Route } from "react-router-dom";
import App from "./App";
import Albums from "./components/Albums";
import Photos from "./components/Photos";
import PhotoDetails from "./components/PhotoDetails";
import Wishlist from "./components/Wishlist";

const Routes = () => {
  return (
    <App>
      <Route exact path="/" component={Albums} />
      <Route exact path="/photo" component={Photos} />
      <Route exact path="/photo/details" component={PhotoDetails} />
      <Route exact path="/wishlist" component={Wishlist} />
    </App>
  );
};

export default Routes;
