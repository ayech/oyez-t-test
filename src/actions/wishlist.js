import { ADDED_TO_WISHLIST, REMOVED_FROM_WISHLIST } from "./actionTypes";

export const addToWishlist = photo => dispatch => {
  dispatch({ type: ADDED_TO_WISHLIST, payload: photo });
};

export const removeFromWishlist = index => dispatch => {
  dispatch({ type: REMOVED_FROM_WISHLIST, payload: index });
};
