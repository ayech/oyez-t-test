import axios from "axios";
import {
  CURRENT_ALBUM,
  FETCH_ALBUMS_FAILURE,
  FETCH_ALBUMS_REQUEST,
  FETCH_ALBUMS_SUCCESS
} from "./actionTypes";

export const fetchAlbums = () => dispatch => {
  dispatch({ type: FETCH_ALBUMS_REQUEST });
  return axios
    .get("https://jsonplaceholder.typicode.com/albums")
    .then(function(response) {
      dispatch({ type: FETCH_ALBUMS_SUCCESS, payload: response.data });
    })
    .catch(function(error) {
      dispatch({ type: FETCH_ALBUMS_FAILURE, error: error.message });
    });
};

export const currentAlbum = album => dispatch => {
  dispatch({ type: CURRENT_ALBUM, payload: album });
};
