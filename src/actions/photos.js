import axios from "axios";
import {
  CURRENT_PHOTO,
  FETCH_PHOTOS_FAILURE,
  FETCH_PHOTOS_REQUEST,
  FETCH_PHOTOS_SUCCESS
} from "./actionTypes";

export const fetchPhotos = albumId => dispatch => {
  dispatch({ type: FETCH_PHOTOS_REQUEST });
  return axios
    .get(`https://jsonplaceholder.typicode.com/photos?albumId=${albumId}`)
    .then(function(response) {
      dispatch({ type: FETCH_PHOTOS_SUCCESS, payload: response.data });
    })
    .catch(function(error) {
      dispatch({ type: FETCH_PHOTOS_FAILURE, error: error.message });
    });
};

export const currentPhoto = photo => dispatch => {
  dispatch({ type: CURRENT_PHOTO, payload: photo });
};
